#include <iostream>
#include "IocContainer.hpp"

#include "include/Comparator/ComparatorInterface.h"
#include "include/Comparator/AscendingComparator.h"
#include "include/Sorter/SorterInterface.h"
#include "include/Sorter/HeapSorter.h"
#include "include/Filler/FillerInterface.h"
#include "include/Filler/RandomFiller.h"
#include "include/Printer/PrinterInterface.h"
#include "include/Printer/ConsolePrinter.h"

using namespace std;

int main() {

    IocContainer container;

    container.registerType<ComparatorInterface, AscendingComparator>();
    container.registerType<SorterInterface, HeapSorter, ComparatorInterface>();
    auto sorter = container.resolve<SorterInterface>();

    container.registerType<FillerInterface, RandomFiller>();
    auto filler = container.resolve<FillerInterface>();

    container.registerType<PrinterInterface, ConsolePrinter>();
    auto printer = container.resolve<PrinterInterface>();


    size_t arrSize = 10;
    auto arr = new int[arrSize];


    filler->fill(arr, arrSize);
    printer->print(arr, arrSize);

    sorter->sort(arr, arrSize);

    printer->print(arr, arrSize);


    delete [] arr;

    return 0;
}