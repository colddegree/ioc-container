#ifndef SORTINGALGORTHMSOOP_RANDOMFILLER_H
#define SORTINGALGORTHMSOOP_RANDOMFILLER_H


#include "FillerInterface.h"

class RandomFiller : public FillerInterface {
private:
    int low;
    int high;
public:
    RandomFiller();

    void fill(int *arr, size_t size) override;
    void setBounds(int low, int high);
};


#endif //SORTINGALGORTHMSOOP_RANDOMFILLER_H
