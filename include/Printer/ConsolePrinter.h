#ifndef SORTINGALGORTHMSOOP_CONSOLEPRINTER_H
#define SORTINGALGORTHMSOOP_CONSOLEPRINTER_H


#include "PrinterInterface.h"

class ConsolePrinter : public PrinterInterface {
private:
    const char *delimiter;
public:
    ConsolePrinter();

    void print(int *arr, size_t size) override;

    void setDelimiter(const char *delimiter);
};


#endif //SORTINGALGORTHMSOOP_CONSOLEPRINTER_H
