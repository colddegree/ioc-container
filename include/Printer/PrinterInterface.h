#ifndef SORTINGALGORTHMSOOP_PRINTERINTERFACE_H
#define SORTINGALGORTHMSOOP_PRINTERINTERFACE_H


#include <cstddef>

class PrinterInterface {
public:
    virtual void print(int *arr, size_t size) = 0;
};


#endif //SORTINGALGORTHMSOOP_PRINTERINTERFACE_H
