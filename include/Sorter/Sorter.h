#ifndef SORTINGALGORTHMSOOP_SORTER_H
#define SORTINGALGORTHMSOOP_SORTER_H


#include "SorterInterface.h"
#include "../Comparator/ComparatorInterface.h"
#include <memory>

class Sorter : public SorterInterface {
protected:
    std::shared_ptr<ComparatorInterface> comparator;
public:
    explicit Sorter(const std::shared_ptr<ComparatorInterface> &comparator);
};


#endif //SORTINGALGORTHMSOOP_SORTER_H
