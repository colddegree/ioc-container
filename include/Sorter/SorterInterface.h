#ifndef SORTINGALGORTHMSOOP_SORTERINTERFACE_H
#define SORTINGALGORTHMSOOP_SORTERINTERFACE_H


#include <cstddef>

class SorterInterface {
public:
    virtual void sort(int *arr, size_t size) = 0;
};


#endif //SORTINGALGORTHMSOOP_SORTERINTERFACE_H
