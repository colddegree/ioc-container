#ifndef SORTINGALGORTHMSOOP_ASCENDINGCOMPARATOR_H
#define SORTINGALGORTHMSOOP_ASCENDINGCOMPARATOR_H


#include "Comparator.h"

class AscendingComparator : public Comparator {
public:
    AscendingComparator();
};


#endif //SORTINGALGORTHMSOOP_ASCENDINGCOMPARATOR_H
