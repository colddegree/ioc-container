#ifndef SORTINGALGORTHMSOOP_COMPARATORINTERFACE_H
#define SORTINGALGORTHMSOOP_COMPARATORINTERFACE_H


class ComparatorInterface {
public:
    virtual int compare(int a, int b) = 0;
    virtual ComparatorInterface& reversed() = 0;
};


#endif //SORTINGALGORTHMSOOP_COMPARATORINTERFACE_H
