#include "../../include/Filler/RandomFiller.h"
#include <cstdlib> // rand
#include <ctime>

RandomFiller::RandomFiller() {
    srand(time(NULL));
    setBounds(0, 9);
}

void RandomFiller::fill(int *arr, size_t size) {
    for (size_t i = 0; i < size; ++i)
        arr[i] = rand() % high + low;
}

void RandomFiller::setBounds(int low, int high) {
    this->low = low;
    this->high = high;
}
