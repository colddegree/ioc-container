#include "../../include/Comparator/AscendingComparator.h"

AscendingComparator::AscendingComparator() {

    compareFunction = [](int a, int b) -> int {
        if (a < b) return -1;
        if (a > b) return 1;
        return 0;
    };
}
